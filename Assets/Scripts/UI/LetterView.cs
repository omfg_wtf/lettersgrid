using TMPro;
using UnityEngine;

namespace UI
{
    public class LetterView : MonoBehaviour
    {
        [SerializeField] private TMP_Text label;
        [SerializeField] private RectTransform self;

        private RectTransform target;
        private Vector3 startPosition;
        private Vector2 startSize;

        public void SetText(string text)
        {
            label.text = text;
        }

        public void SetTarget(RectTransform targetTransform)
        {
            this.target = targetTransform;
            this.startPosition = self.position;
            this.startSize = self.sizeDelta;
        }

        public void PlayFitCellAnimation(float progress)
        {
            self.position = Vector3.Lerp(startPosition, target.position, progress);
            self.sizeDelta = Vector2.Lerp(startSize, target.sizeDelta, progress);
        }
    }
}