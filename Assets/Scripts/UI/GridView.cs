using Input;
using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using UnityEngine;
using UnityEngine.UI;

namespace UI
{
    public class GridView : MonoBehaviour
    {
        public event Action OnGridDimensionChange = delegate { };
        public event Action OnGridRectChange = delegate { };

        public int Width { get; private set; }
        public int Height { get; private set; }
        public ReadOnlyCollection<RectTransform> Cells
        {
            get { return roCells; }
        }

        private RectTransform rTForm;
        private GridLayoutGroup grid;
        private List<RectTransform> cells;
        private ReadOnlyCollection<RectTransform> roCells;

        [SerializeField] private InputService input;
        private void Awake()
        {
            cells = new List<RectTransform>();
            roCells = cells.AsReadOnly();
            rTForm = GetComponent<RectTransform>();
            grid = GetComponent<GridLayoutGroup>();

            grid.constraint = GridLayoutGroup.Constraint.FixedColumnCount;

            OnRectTransformDimensionsChange();
        }

        private void OnEnable() => input.OnGenerateClick += ChangeGridSize;

        private void OnDisable() => input.OnGenerateClick -= ChangeGridSize;

        private void ChangeGridSize(int width, int height)
        {
            if (width < 0 || height < 0) throw new ArgumentException($"width:{width} height:{height}");
            if (this.Width == width && this.Height == height) return;

            this.Width = width;
            this.Height = height;

            grid.constraintCount = width;

            UpdateGrid();

            OnGridDimensionChange();
        }

        private void UpdateGrid()
        {
            int newCellCount = Width * Height;
            int max = Mathf.Max(cells.Count, newCellCount);
            for (int i = 0; i < max; i++)
            {
                if (i >= cells.Count)
                {
                    cells.Add(CreateCell());
                }
                cells[i].gameObject.SetActive(i < newCellCount);
            }

            OnRectTransformDimensionsChange();
        }

        private RectTransform CreateCell()
        {
            var go = new GameObject(cells.Count.ToString(), typeof(RectTransform));
            var rtf = go.GetComponent<RectTransform>();
            rtf.SetParent(rTForm, true);
            rtf.localScale = Vector3.one;
            return rtf;
        }

        private void OnRectTransformDimensionsChange()
        {
            if (!rTForm || !grid) return;
            if (Width <= 0 || Height <= 0) return;
            float size = Mathf.Min(
                rTForm.rect.width / Width,
                rTForm.rect.height / Height);
            grid.cellSize = new Vector2(size, size);

            LayoutRebuilder.ForceRebuildLayoutImmediate(rTForm);

            OnGridRectChange();
        }
    }
}