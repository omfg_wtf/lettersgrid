using Input;
using Extensions;
using UnityEngine;
using System.Collections.Generic;

namespace UI
{
    public class LettersView : MonoBehaviour
    {
        private List<LetterView> letters = new List<LetterView>();
        private int count;

        [SerializeField] private GridView grid;
        [SerializeField] private InputService input;

        [SerializeField] private float shuffleAnimationLength = 2f;
        private float �nimationProgress = 1f;

        public void Update()
        {
            if(�nimationProgress < 1f)
            {
                �nimationProgress += Time.deltaTime / shuffleAnimationLength;
                MoveAllLetters();
            }
        }

        private void Generate()
        {
            count = grid.Width * grid.Height;

            int max = Mathf.Max(letters.Count, count);
            for (int i = 0; i < max; i++)
            {
                if (i >= letters.Count)
                {
                    letters.Add(CreateText());
                    letters[i].SetTarget(grid.Cells[i]);
                }
                letters[i].SetText(RandomLetter());
                letters[i].gameObject.SetActive(i < count);
            }

            MoveAllLetters();
        }

        private void Shuffle()
        {
            letters.Shuffle(count);

            for (int i = 0; i < count; i++)
            {
                letters[i].SetTarget(grid.Cells[i]);
            }

            �nimationProgress = 0f;
        }

        private void MoveAllLetters()
        {
            for (int i = 0; i < count; i++)
            {
                letters[i].PlayFitCellAnimation(�nimationProgress);
            }
        }

        private LetterView CreateText()
        {
            var result = Instantiate(Resources.Load<GameObject>("Prefabs/Letter")).GetComponent<LetterView>();
            result.transform.SetParent(transform, true);
            result.transform.localScale = Vector3.one;
            return result;
        }

        private string RandomLetter()
        {
            return ((char)Random.Range('A', 'Z')).ToString();
        }

        private void OnEnable()
        {
            input.OnShuffleClick += Shuffle;
            grid.OnGridRectChange += MoveAllLetters;
            grid.OnGridDimensionChange += Generate;
        }

        private void OnDisable()
        {
            input.OnShuffleClick -= Shuffle;
            grid.OnGridRectChange -= MoveAllLetters;
            grid.OnGridDimensionChange -= Generate;
        }
    }
}