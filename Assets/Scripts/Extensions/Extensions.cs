using UnityEngine;
using System.Collections.Generic;

namespace Extensions
{
    static class Extensions
    {
        public static void Shuffle<T>(this List<T> list, int count)
        {
            for (int i = 0; i < count; i++)
            {
                int n = Random.Range(0, count);
                T temp = list[i];
                list[i] = list[n];
                list[n] = temp;
            }
        }
    }
}