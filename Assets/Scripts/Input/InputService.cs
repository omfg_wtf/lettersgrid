using System;
using UnityEngine;
using UnityEngine.UI;
using Validation;

namespace Input
{
    public class InputService : MonoBehaviour
    {
        [SerializeField] private ValidatableInputField widthInputField;
        [SerializeField] private ValidatableInputField heightInputField;
        [SerializeField] private Button generateButton;
        [SerializeField] private Button shuffleButton;

        public event Action<int, int> OnGenerateClick = delegate { };
        public event Action OnShuffleClick = delegate { };

        private void OnEnable()
        {
            generateButton.onClick.AddListener(GenerateBttnHandler);
            shuffleButton.onClick.AddListener(ShuffleBttnHandler);
        }

        private void OnDisable()
        {
            generateButton.onClick.RemoveListener(GenerateBttnHandler);
            shuffleButton.onClick.RemoveListener(ShuffleBttnHandler);
        }

        private void GenerateBttnHandler()
        {
            int w, h;
            if (!widthInputField.TryGetValidValue(GetPositiveInteger, out w)) return;
            if (!heightInputField.TryGetValidValue(GetPositiveInteger, out h)) return;

            OnGenerateClick(w, h);
        }

        private void ShuffleBttnHandler()
        {
            OnShuffleClick();
        }

        private bool GetPositiveInteger(string input, out int validValue)
        {
            bool result = int.TryParse(input, out validValue);
            result &= validValue >= 0;
            return result;
        }
    }
}